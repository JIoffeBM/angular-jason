import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';

let componentName = 'bmSearchResults';

class SearchComponentController{
    q: string;
    products: IProduct[];

    //Define dependencies for DI, which are automatically passed
    //in order through the constructor below
    static $inject = [
        productServiceName
    ];

    constructor(private productService: IProductService) { }

    $routerOnActivate(next) {
        this.q = next.params.q;

        this.productService.getProductsByName(this.q).then(data => this.products = data);
    }
}

let adminComponent: IComponentOptions = {
    templateUrl: 'app/search-results/search-results.component.html',
    controller: SearchComponentController,
    controllerAs: 'searchResults'
};

module(ModuleName).component(componentName, adminComponent);

export const ComponentName = componentName;