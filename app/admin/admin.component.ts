import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';

let componentName = 'bmAdmin';

class AdminComponentController{
    products: IProduct[];

    //Define dependencies for DI, which are automatically passed
    //in order through the constructor below
    static $inject = [
        productServiceName
    ];

    constructor(private productService: IProductService) { }

    $routerOnActivate() {
        //Doesn't require any real data passed from the router snapshot,
        //but this code will run once this component is brought
        //to focus by the router
        this.getProducts();
    }    

    public getProducts(){
        this.productService.getProducts().then(value => this.products = value);
    }
    public createNewProduct(){
        this.productService.createProduct().then( () => this.getProducts() );
    }

    public saveProduct(product: IProduct){
        this.productService.updateProduct(product).then( () => this.getProducts() );
    }

    public deleteProduct(id: string){
        this.productService.deleteProduct(id).then( () => this.getProducts() );
    }
}

let adminComponent: IComponentOptions = {
    templateUrl: 'app/admin/admin.component.html',
    controller: AdminComponentController,
    controllerAs: 'admin'
};

module(ModuleName).component(componentName, adminComponent);

export const ComponentName = componentName;