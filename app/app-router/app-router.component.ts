import { module, IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import {ComponentName as productListComponentName} from '../product-list/product-list.component';
import {ComponentName as productDetailComponentName} from '../product-detail/product-detail.component';
import {ComponentName as aboutComponentName} from '../about/about.component';
import {ComponentName as adminComponentName} from '../admin/admin.component';
import {ComponentName as searchResultsComponentName} from '../search-results/search-results.component';
import {ComponentName as cartComponentName} from '../cart/cart.component';

let componentName = 'bmAppRouter';

let appRouterComponent: IComponentOptions = {
    transclude: true,
    templateUrl: 'app/app-router/app-router.component.html',
    $routeConfig: [
        { path: '/products', name: 'Products', component: productListComponentName, useAsDefault: true },
        { path: '/product/:id', name: 'Product', component: productDetailComponentName },
        { path: '/about', name: 'About', component: aboutComponentName },
        { path: '/admin', name: 'Admin', component: adminComponentName },
        { path: '/search', name:'Search', component: searchResultsComponentName},
        { path: '/search/:q', name:'Search', component: searchResultsComponentName},
        { path: '/cart', name: 'Cart', component: cartComponentName}
    ]
};

module(ModuleName).component(componentName, appRouterComponent);

export const ComponentName = componentName;



