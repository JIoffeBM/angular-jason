import { module, IQService, IPromise, IWindowService } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';

const PRODUCTS_LS_LOCATION = 'bm-product-list';
const ID_COUNTER_LS_LOCATION = 'bm-product-id-counter';

//Keep a list of supported keys to ensure data integrity
//When passing an array to and from an applied model,
//angular may add a few extra properties for keeping track
//of unique entries, etc.
const PROPERTIES = [
    'name',
    'price'
]

let serviceName = 'productService';

/*
    The interface exposed to the application includes the following
    self-explanitory functions.

    In this simple setting, 'get' methods will return null if the
    user agent does not support local storage. In the real world,
    an engineer can either check for validity of the returned data
    from the promise, use the optional syntax in Angular templating,
    or return a "dummy" object that will hold no information but
    prevent null pointer exceptions
*/
export interface IProductService {
    getProducts(): IPromise<IProduct[]>;
    getProductMap(): IPromise<IProductMap>;
    createProduct(): IPromise<IProduct>;
    updateProduct(product: IProduct);
    getProductById(id: string): IPromise<IProduct>;
    getProductsByName(name: string): IPromise<IProduct[]>;
    deleteProduct(id: string): IPromise<IProduct>;
}

/*
    The IProductMap stored on local storage consists of two objects:
    - a timestamp
    - an object whose keys correspond with the unique IDs of products, and values are products
*/
export interface IProductMap{
    timestamp: Object,
    map: Object
}
class ProductService implements IProductService {

    static $inject = [
        '$q',
        '$window'
    ];

    /*
        Interface Implementations
    */
    getProducts(): IPromise<IProduct[]> {
        if(!this.validConnection())
            return this.$q.when([]);

        let productMap: IProductMap = <IProductMap>this.retrieve(PRODUCTS_LS_LOCATION);
        let products = this.productMapToArray(productMap);

        return this.$q.when(products);
    }

    getProductMap(): IPromise<IProductMap>{
        if(!this.validConnection())
            return this.$q.when(null);

        let productMap: IProductMap = <IProductMap>this.retrieve(PRODUCTS_LS_LOCATION);
        return this.$q.when(productMap);
    }

    getProductById(id: string): IPromise<IProduct>{
        if(!this.validConnection())
            return this.$q.when(null);

        let productMap: IProductMap = <IProductMap>this.retrieve(PRODUCTS_LS_LOCATION);
        return this.$q.when(productMap.map[id] || null);
    }

    deleteProduct(id: string): IPromise<IProduct>{
        if(!this.validConnection())
            return this.$q.when(null);

        let productMap: IProductMap = <IProductMap>this.retrieve(PRODUCTS_LS_LOCATION);
        let product: IProduct = productMap.map[id] || null;
        delete productMap.map[id];
        this.saveProductMap(productMap);

        return this.$q.when(product);
    }

    getProductsByName(name: string): IPromise<IProduct[]>{
        if(!this.validConnection())
            return this.$q.when(null);

        if(!name || !name.length)
            return this.$q.when([]);

        name = name.toLowerCase();

        let productMap: IProductMap = <IProductMap>this.retrieve(PRODUCTS_LS_LOCATION);
        let map = productMap.map;

        let products = <IProduct[]>Object.keys(map)
                                        .map(key => map[key])
                                        .filter(product => product.name.toLowerCase().indexOf(name) > -1);

        return this.$q.when(products);      
    }

    createProduct(): IPromise<IProduct>{
        if(!this.validConnection())
            return this.$q.when(null);

        //Simple ID counter to prevent duplicates
        //In this case we just keep a running integer count on local storage,
        //other methods I considered for this project include
        //combinations of the hashcode of the current product and the current datetime value,
        //or otherwise generate a string of uniform length for each entry
        let currentId: number = (<number>this.retrieve(ID_COUNTER_LS_LOCATION) || 0) + 1;
        let product: IProduct = {id: currentId, price: 0, name: 'Untitled'};
        let productMap: IProductMap = <IProductMap>this.retrieve(PRODUCTS_LS_LOCATION);

        productMap.map[currentId] = product;

        this.saveProductMap(productMap);
        this.commit(ID_COUNTER_LS_LOCATION, currentId);

        return this.$q.when(product);
    }

    updateProduct(product: IProduct): IPromise<IProduct>{
        if(!this.validConnection())
            return this.$q.when(null);

        let productMap: IProductMap = <IProductMap>this.retrieve(PRODUCTS_LS_LOCATION);
        let map = productMap.map;
        let id = product.id;
        
        PROPERTIES.forEach(key => map[id][key] = product[key]);

        this.saveProductMap(productMap);

        return this.$q.when(product);   
    }

    /*
        Helper Functions
    */
    private validConnection(): boolean{
        if(!this.$window.localStorage){
            console.log('Local storage is not supported by this browser!');
            return false;
        }

        return true;
    }
    private commit(destination: string, data: Object): void{
        this.$window.localStorage.setItem(destination, JSON.stringify(data));
    }
    private retrieve(source: string): Object{
        if(!this.validConnection())
            return null;

        return JSON.parse(this.$window.localStorage.getItem(source));
    }
    private saveProductMap(productMap: IProductMap): void{
        productMap.timestamp = new Date();
        this.commit(PRODUCTS_LS_LOCATION, productMap);
    }
    private productMapToArray(productMap: IProductMap): IProduct[]{
        let map = productMap.map;
        return Object.keys(map).map(key => map[key]);
    }

    constructor(private $q: IQService, private $window: IWindowService) { }
}

module(ModuleName).service(serviceName, ProductService);

export const ServiceName = serviceName;

