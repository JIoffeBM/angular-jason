/**
 *  cart.service.ts
 *  Supports accessing the current user's cart for display, purchase, or cancellations
 */
import { module, IQService, IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';
import { ServiceName as productServiceName, IProductService, IProductMap } from '../services/product.service';

const CART_LS_LOCATION = 'bm-cart-data';

let serviceName = 'cartService';

/*
    ICartSession
    This represents the status of the current cart,
    including all the items and a timestamp for last
    modification.

    products is a hash table of product IDs paired with an integer of the count
*/
export interface ICartSession{
    timestamp: Object;
    products: Object;
}
export interface ICartService {
    addProduct(id: string): IPromise<ICartSession>;
    removeProduct(id: string): IPromise<ICartSession>;
    getSession(): IPromise<ICartSession>;
    clear(): IPromise<ICartSession>;
}

class CartService implements ICartService {
    static $inject = [
        '$q',
        productServiceName
    ];

    getSession(): IPromise<ICartSession>{
        let cartSession = this.getActiveSession();
        return this.$q.when(this.validateSession(cartSession));
    }

    addProduct(id: string): IPromise<ICartSession>{
        let cartSession = this.getActiveSession();
        let products = cartSession.products;

        if(!products[id])
            products[id] = 1;
        else
            products[id]++;

        return this.saveSession(cartSession).then( () => cartSession);
    }

    removeProduct(id: string): IPromise<ICartSession>{
        let cartSession = this.getActiveSession();
        let products = cartSession.products;

        if(!!products[id]){
            if(products[id] > 1)
                products[id]--;
            else
                delete products[id];
        }
            
        this.saveSession(cartSession);

        return this.saveSession(cartSession).then( () => cartSession);   
    }

    clear(): IPromise<ICartSession>{
        let cartSession = this.createNewSession();
        return this.$q.when(cartSession);
    }

    /*
        getActiveSession()
        Will return either the current active session,
        or initialize the active session to an empty cart if it does not exist
    */
    private getActiveSession(): ICartSession{
        if(!window.localStorage){
            console.log('Local storage is not supported by this browser!');
            return null;
        }

        let activeSessionData = window.localStorage.getItem(CART_LS_LOCATION);

        if(!!activeSessionData)
            return JSON.parse(activeSessionData);

        let newSession = this.createNewSession();

        return newSession;
    }
    private saveSession(cartSession: ICartSession): IPromise<ICartSession>{
        cartSession.timestamp = new Date();
        window.localStorage.setItem(CART_LS_LOCATION, JSON.stringify(cartSession));
        return this.$q.when(cartSession);
    }
    private createNewSession(): ICartSession{
        let cartSession:ICartSession =  {
            timestamp: new Date(),
            products: {}
        }

        window.localStorage.setItem(CART_LS_LOCATION, JSON.stringify(cartSession));
        return cartSession;
    }
    private validateSession(cartSession: ICartSession): IPromise<ICartSession>{
        /*
            Validates the cart by deleting
            all keys which are not available in the active Product Map

            Step 1: Get the product map from the products service
            Step 2: filter for keys that are not in the product map and delete them from the cart
            Step 3: commit validation to storage medium
        */       
        return this.productService.getProductMap()
            .then( productMap => {
                let map = productMap.map;
                let cartProducts = cartSession.products;
                Object.keys(cartProducts).filter(key => !map[key]).forEach(key => delete cartProducts[key]);
                return cartSession;
            })
            .then( validatedSession => this.saveSession(validatedSession));
    }
    constructor(private $q: IQService, private productService: IProductService) { }
}

module(ModuleName).service(serviceName, CartService);

export const ServiceName = serviceName;