import { module, IComponentOptions} from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../';

let componentName = 'bmSearchWidget';


class SearchWidgetComponentController {
    q: string = '';
}

let searchWidgetComponent: IComponentOptions = {
    templateUrl: 'app/search-widget/search-widget.component.html',
    controller: SearchWidgetComponentController,
    controllerAs: 'searchWidget'
};

module(ModuleName).component(componentName, searchWidgetComponent);

export const ComponentName = componentName;