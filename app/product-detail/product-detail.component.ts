import { module, IComponentOptions, IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';
import {ServiceName as cartServiceName, ICartService, ICartSession } from '../services/cart.service';
import {ComponentName as appRouterComponentName} from '../app-router/app-router.component';

let componentName = 'bmProduct';

class ProductComponentController {
    product: IProduct;
    cartSession: ICartSession;

    static $inject = [
        productServiceName,
        cartServiceName
    ];

    constructor(private productService: IProductService, private cartService: ICartService) {}

    $routerOnActivate(next) {
        this.productService.getProductById(next.params.id).then(product => this.product = product);
        this.cartService.getSession().then(cartSession => this.cartSession = cartSession);
    }

    public onAddToCart(): IPromise<ICartSession>{
        return this.cartService.addProduct(this.product.id + '').then(cartSession => this.cartSession = cartSession);
    }

    public numberInCart(): number{
        return this.cartSession.products[this.product.id] || 0;
    }
}

let productComponent: IComponentOptions = {
    templateUrl: 'app/product-detail/product-detail.component.html',
    controller: ProductComponentController,
    controllerAs: 'productDetail'
};

module(ModuleName).component(componentName, productComponent);

export const ComponentName = componentName;