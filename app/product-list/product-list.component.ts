import { module,
    IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';

let componentName = 'bmProductList';

class ProductListComponentController {

    static $inject = [
        productServiceName
    ];

    products: IProduct[];

    constructor(private productService: IProductService) { }

    $routerOnActivate() {
        this.productService.getProducts().then(value => this.products = value);
    }
}

let productListComponent: IComponentOptions = {
    templateUrl: 'app/product-list/product-list.component.html',
    controller: ProductListComponentController,
    controllerAs: 'productList'
};

module(ModuleName).component(componentName, productListComponent);

export const ComponentName = componentName;