import { module, IComponentOptions, IPromise } from 'angular';
import { ModuleName } from '../app';

import {ServiceName as productServiceName, IProductService, IProductMap } from '../services/product.service';
import {ServiceName as cartServiceName, ICartService, ICartSession } from '../services/cart.service';

let componentName = 'bmCart';

class CartComponentController{
    cartSession: ICartSession;
    productMap: IProductMap;

    static $inject = [
        productServiceName,
        cartServiceName
    ];

    constructor(private productService: IProductService, private cartService: ICartService) {}

    $routerOnActivate() {
        this.productService.getProductMap().then(productMap => this.productMap = productMap);
        this.cartService.getSession().then(cartSession => this.cartSession = cartSession);
    }

    onProductRemove(id: string): IPromise<ICartSession>{
        return this.cartService.removeProduct(id).then(cartSession => this.cartSession = cartSession);
    }

    onCartEmpty(): IPromise<ICartSession>{
        return this.cartService.clear().then(cartSession => this.cartSession = cartSession);
    }

    isEmpty(){
        return !this.cartService || Object.keys(this.cartSession.products).length === 0;
    }
}
let cartComponent: IComponentOptions = {
    templateUrl: 'app/cart/cart.component.html',
    controller: CartComponentController,
    controllerAs: 'cart'
};

module(ModuleName).component(componentName, cartComponent);

export const ComponentName = componentName;